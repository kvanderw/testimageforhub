##################################
# Dockerfile to build a test image
##################################
# Base image is Ubuntu 14.04
FROM ubuntu:14.04
# Author Kurt
MAINTAINER  Kurt L Vanderwater <kurt@meridian-ds.com>
# create mynewdir and mynewfile
RUN mkdir /mynewdir; echo 'this is my new container to make image and then push to hub'> /mynewdir/mynewfile
